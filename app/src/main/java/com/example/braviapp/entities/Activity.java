package com.example.braviapp.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Activity extends RealmObject implements Parcelable {

    public static final int STATUS_NEW = 0;
    public static final int STATUS_IN_PROGRESS = 1;
    public static final int STATUS_FINALIZED = 2;
    public static final int STATUS_GIVE_UP = 3;

    @PrimaryKey
    private String key;
    private String activity;
    private Double accessibility;
    private String type;
    private Integer participants;
    private Double price;
    private String link;

    private Date startedAt;

    private Date finishedAt;

    private String wastedTime;

    private Integer status = STATUS_NEW;

    private Date createdAt = new Date();

    public Activity() {

    }

    protected Activity(Parcel in) {
        key = in.readString();
        activity = in.readString();
        if (in.readByte() == 0) {
            accessibility = null;
        } else {
            accessibility = in.readDouble();
        }
        type = in.readString();
        if (in.readByte() == 0) {
            participants = null;
        } else {
            participants = in.readInt();
        }
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readDouble();
        }
        link = in.readString();
        wastedTime = in.readString();
        if (in.readByte() == 0) {
            status = null;
        } else {
            status = in.readInt();
        }
    }

    public static final Creator<Activity> CREATOR = new Creator<Activity>() {
        @Override
        public Activity createFromParcel(Parcel in) {
            return new Activity(in);
        }

        @Override
        public Activity[] newArray(int size) {
            return new Activity[size];
        }
    };

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Double getAccessibility() {
        return accessibility;
    }

    public void setAccessibility(Double accessibility) {
        this.accessibility = accessibility;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getParticipants() {
        return participants;
    }

    public void setParticipants(Integer participants) {
        this.participants = participants;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public Date getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Date finishedAt) {
        this.finishedAt = finishedAt;
    }

    public String getWastedTime() {
        return wastedTime;
    }

    public void setWastedTime(String wastedTime) {
        this.wastedTime = wastedTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(key);
        parcel.writeString(activity);
        if (accessibility == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(accessibility);
        }
        parcel.writeString(type);
        if (participants == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(participants);
        }
        if (price == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(price);
        }
        parcel.writeString(link);
        parcel.writeString(wastedTime);
        if (status == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(status);
        }
    }

    public String getStatusName() {
        switch (this.getStatus()) {
            case STATUS_NEW:
                return "Novo";

            case STATUS_IN_PROGRESS:
                return "Em andamento";

            case STATUS_FINALIZED:
                return "Finalizada";

            case STATUS_GIVE_UP:
                return "Desistência";

            default:
                return "";
        }
    }
}
