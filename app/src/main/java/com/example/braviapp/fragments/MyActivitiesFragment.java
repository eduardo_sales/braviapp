package com.example.braviapp.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.braviapp.R;
import com.example.braviapp.adapters.ActivityListAdapter;
import com.example.braviapp.adapters.MyActivitiesListAdapter;
import com.example.braviapp.databinding.FragmentMyActivitiesBinding;
import com.example.braviapp.entities.Activity;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class MyActivitiesFragment extends Fragment {

    private FragmentMyActivitiesBinding binding;
    private Realm realm;
    private MyActivitiesListAdapter mAdapter;
    private final ArrayList<Activity> activityList = new ArrayList<>();
    private RealmResults<Activity> realmResults;

    public MyActivitiesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentMyActivitiesBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        getActivity().setTitle(R.string.my_activities);

        realm = Realm.getDefaultInstance();

        realmResults = realm.where(Activity.class)
                .notEqualTo("status", Activity.STATUS_NEW)
                .sort("status", Sort.ASCENDING)
                .findAll();

        realmResults.addChangeListener(activities -> populateRecyclerView(activities));

        RecyclerView mRecycleView = binding.myActivitiesList;
        FragmentManager fm = getActivity().getSupportFragmentManager();
        mAdapter = new MyActivitiesListAdapter(getContext(), fm, activityList);
        mRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecycleView.setItemAnimator(new DefaultItemAnimator());
        mRecycleView.setHasFixedSize(true);
        mRecycleView.setAdapter(mAdapter);

        if (mAdapter.getItemCount() == 0 && realmResults.size() > 0) {
            populateRecyclerView(realmResults);
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realmResults.removeAllChangeListeners();
        realm.close();
    }

    private void populateRecyclerView(RealmResults<Activity> activities) {

        activityList.clear();

        if(activities.size() > 0) {
            activityList.addAll(activities);
        }

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }
}