package com.example.braviapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.braviapp.databinding.FragmentActivityDetailBinding;
import com.example.braviapp.entities.Activity;

import java.util.Date;

import io.realm.Realm;

public class ActivityDetailFragment extends Fragment {

    private FragmentActivityDetailBinding binding;
    private Realm realm;
    private Activity activity;

    public ActivityDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentActivityDetailBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        realm = Realm.getDefaultInstance();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            activity = bundle.getParcelable("activity");
        }

        getActivity().setTitle("Atividade #" + activity.getKey());

        binding.description.setText(activity.getActivity());

        binding.type.setText(activity.getType());

        binding.accessibility.setText(String.valueOf(activity.getAccessibility()));

        binding.participants.setText(String.valueOf(activity.getParticipants()));

        binding.price.setText(String.valueOf(activity.getPrice()));

        binding.btnStart.setOnClickListener(view1 -> {
            realm.executeTransaction(realm -> {
                activity.setStartedAt(new Date());
                activity.setStatus(Activity.STATUS_IN_PROGRESS);
            });
            showHideElements();
        });

        binding.btnFinish.setOnClickListener(view12 -> {
            finishActivity(Activity.STATUS_FINALIZED);
        });

        binding.btnGiveup.setOnClickListener(view13 -> {
            finishActivity(Activity.STATUS_GIVE_UP);
        });

        showHideElements();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private String calcWastedTime(Date startedAt, Date finishedAt) {
        String result = "";

        try {
            long diffMillis = finishedAt.getTime() - startedAt.getTime();

            long diffInSeconds = (diffMillis / 1000) % 60;

            long diffInMinutes = (diffMillis / (1000 * 60)) % 60;

            long diffInHours = (diffMillis / (1000 * 60 * 60)) % 24;

            result = (diffInHours < 10 ? ("0" + diffInHours) : diffInHours) +":";
            result += (diffInMinutes < 10 ? ("0" + diffInMinutes) : diffInMinutes) +":";
            result += (diffInSeconds < 10 ? ("0" + diffInSeconds) : diffInSeconds);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private void finishActivity(int status) {
        Date finishedAt = new Date();
        String wastedTime = this.calcWastedTime(activity.getStartedAt(), finishedAt);

        realm.executeTransaction(realm -> {
            activity.setWastedTime(wastedTime);
            activity.setFinishedAt(finishedAt);
            activity.setStatus(status);
        });

        getActivity().getSupportFragmentManager().popBackStack();
    }

    private void showHideElements() {
        switch (activity.getStatus()) {
            case Activity.STATUS_NEW:
                binding.btnStart.setVisibility(View.VISIBLE);
                binding.layoutActions.setVisibility(View.GONE);
                break;

            case Activity.STATUS_IN_PROGRESS:
                binding.btnStart.setVisibility(View.GONE);
                binding.layoutActions.setVisibility(View.VISIBLE);
                break;

            case Activity.STATUS_FINALIZED | Activity.STATUS_GIVE_UP:
                binding.btnStart.setVisibility(View.GONE);
                binding.layoutActions.setVisibility(View.GONE);
                break;
        }
    }
}