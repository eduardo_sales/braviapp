package com.example.braviapp.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.braviapp.R;
import com.example.braviapp.adapters.ActivityListAdapter;
import com.example.braviapp.api.services.ActivityService;
import com.example.braviapp.databinding.FragmentSuggestionsBinding;
import com.example.braviapp.entities.Activity;
import com.example.braviapp.utils.ActivityTypes;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

public class SuggestionsFragment extends Fragment {

    private FragmentSuggestionsBinding binding;
    private Realm realm;
    private ActivityListAdapter mAdapter;
    private final ArrayList<Activity> activityList = new ArrayList<>();
    private RealmResults<Activity> realmResults;

    public SuggestionsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSuggestionsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        getActivity().setTitle(getString(R.string.suggested_activities));

        realm = Realm.getDefaultInstance();

        realmResults = realm.where(Activity.class)
                .equalTo("status", Activity.STATUS_NEW)
                .findAll();

        realmResults.addChangeListener(activities -> populateRecyclerView(activities));

        ArrayList<ActivityTypes> listTypes = new ArrayList<>(EnumSet.allOf(ActivityTypes.class));
        ArrayAdapter<ActivityTypes> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, listTypes);
        binding.spType.setAdapter(adapter);

        binding.btnFilter.setOnClickListener(view1 -> {
            String mType = binding.spType.getSelectedItem().toString();
            if (mType.isEmpty()) {
                Toast.makeText(getContext(),
                        "Selecione um tipo para filtrar",
                        Toast.LENGTH_SHORT).show();
                return;
            }

            final ProgressDialog progress = new ProgressDialog(getContext());
            progress.setTitle(getString(R.string.wait));
            progress.setMessage(getString(R.string.sync_data));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setMax(100);
            progress.setCancelable(false);
            progress.setCanceledOnTouchOutside(false);
            progress.show();

            new Thread() {
                @Override
                public void run() {
                    try {
                        ActivityService.getRandom(mType.toLowerCase(Locale.ROOT));
                        sleep(2000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        progress.dismiss();
                    }
                }

            }.start();
        });

        RecyclerView mRecycleView = binding.suggestionList;
        FragmentManager fm = getActivity().getSupportFragmentManager();
        mAdapter = new ActivityListAdapter(getContext(), fm, activityList);
        mRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecycleView.setItemAnimator(new DefaultItemAnimator());
        mRecycleView.setHasFixedSize(true);
        mRecycleView.setAdapter(mAdapter);

        if (mAdapter.getItemCount() == 0 && realmResults.size() > 0) {
            populateRecyclerView(realmResults);
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realmResults.removeAllChangeListeners();
        realm.close();
    }

    private void populateRecyclerView(RealmResults<Activity> activities) {

        activityList.clear();

        if(activities.size() > 0) {
            activityList.addAll(activities);
        }

        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }
}