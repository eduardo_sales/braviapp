package com.example.braviapp.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.braviapp.R;
import com.example.braviapp.databinding.ActivityMainBinding;
import com.example.braviapp.fragments.MyActivitiesFragment;
import com.example.braviapp.fragments.SuggestionsFragment;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity implements NavigationBarView.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        MainActivity.this.setTitle(getString(R.string.activities));

        // bottom navigation menu
        binding.bottomNav.setOnItemSelectedListener(this);

        SuggestionsFragment fragment = new SuggestionsFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment selectedFragment = null;
        int itemId = item.getItemId();

        if (itemId == R.id.suggestionsMenu) {
            selectedFragment = new SuggestionsFragment();
        } else if (itemId == R.id.myActivitiesMenu) {
            selectedFragment = new MyActivitiesFragment();
        }

        if (selectedFragment != null) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, selectedFragment);
            ft.commit();
        }

        return true;
    }
}