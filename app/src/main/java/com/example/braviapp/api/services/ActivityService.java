package com.example.braviapp.api.services;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.braviapp.api.requests.ActivityRequest;
import com.example.braviapp.api.routes.ActivityAPI;
import com.example.braviapp.entities.Activity;
import com.example.braviapp.utils.RetrofitConfig;
import com.google.gson.Gson;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityService {

    public static void getRandom(String type) {
        Retrofit retrofit = new RetrofitConfig()
                .getRetrofit();

        Call<ActivityRequest> call;

        if (type == null || type.isEmpty()) {
            call = retrofit.create(ActivityAPI.class).getRandom();
        } else {
            call = retrofit.create(ActivityAPI.class).getByType(type);
        }

        call.enqueue(new Callback<ActivityRequest>() {
            @Override
            public void onResponse(@NonNull Call<ActivityRequest> call,
                                   @NonNull Response<ActivityRequest> response) {

                ActivityRequest responseBody = response.body();

                if (response.isSuccessful() && responseBody != null) {
                    Realm realm = Realm.getDefaultInstance();
                    try {
                        Activity existActivity = realm.where(Activity.class)
                                .equalTo("key", responseBody.getKey()).findFirst();

                        if (existActivity == null) {
                            realm.executeTransaction(realm1 -> realm1.createObjectFromJson(
                                    Activity.class,
                                    new Gson().toJson(responseBody))
                            );
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        realm.close();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ActivityRequest> call, @NonNull Throwable t) {
                Log.d("DEV", "ActiviyService - Request error: " + t.getMessage());
                t.printStackTrace();
            }
        });
    }
}
