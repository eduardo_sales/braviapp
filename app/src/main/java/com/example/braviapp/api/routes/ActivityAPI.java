package com.example.braviapp.api.routes;

import com.example.braviapp.api.requests.ActivityRequest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ActivityAPI {

    @GET("activity/")
    Call<ActivityRequest> getRandom();

    @GET("activity")
    Call<ActivityRequest> getByType(@Query("type") String type);
}
