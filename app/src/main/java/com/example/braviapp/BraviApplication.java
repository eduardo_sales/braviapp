package com.example.braviapp;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BraviApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize Realm
        Realm.init(getApplicationContext());

        // Inicializa Configuracoes do Realm
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("bravidb")
                .deleteRealmIfMigrationNeeded()
                .allowWritesOnUiThread(true)
                .allowQueriesOnUiThread(true)
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
