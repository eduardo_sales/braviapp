package com.example.braviapp.utils;

public enum ActivityTypes {

    EDUCATION("education"),
    RECREATIONAL("recreational"),
    SOCIAL("social"),
    DIY("diy"),
    CHARITY("charity"),
    COOKING("cooking"),
    RELAXATION("relaxation"),
    MUSIC("music"),
    BUSYWORK("busywork");

    private final String type;

    ActivityTypes(String mType) {
        type = mType;
    }

    public String getType() {
        return type;
    }
}
