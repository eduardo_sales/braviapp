package com.example.braviapp.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Retrofit configuration
 */
public class RetrofitConfig {

    public RetrofitConfig() {

    }

    public Retrofit getRetrofit(Boolean... params) {
//        boolean sendCredentials;
//
//        if (params.length == 0) {
//            sendCredentials = true;
//        } else {
//            sendCredentials = params[0];
//        }

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient.Builder builder = new OkHttpClient
                .Builder()
                .addInterceptor(logging)
                .connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES);


        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {

                Request.Builder builder = chain.request()
                        .newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept", "application/json");

                // envia as credencias
//                if (sendCredentials) {
//                    builder.addHeader("Authorization", "Bearer ");
//                }

                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        });


        OkHttpClient client = builder.build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        return new Retrofit.Builder()
                .baseUrl("https://www.boredapi.com/api/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

}
