package com.example.braviapp.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.braviapp.R;
import com.example.braviapp.databinding.ActivityListItemBinding;
import com.example.braviapp.entities.Activity;
import com.example.braviapp.fragments.ActivityDetailFragment;

import java.util.List;

/** RecyclerView adapter **/
public class ActivityListAdapter extends RecyclerView.Adapter<ActivityListAdapter.ActivityListViewHolder> {

    private Context ctx;
    private FragmentManager fm;
    private List<Activity> activityList;
    private ActivityListItemBinding binding;

    public ActivityListAdapter(Context context, FragmentManager fm, List<Activity> activityList) {
        this.ctx = context;
        this.fm = fm;
        this.activityList = activityList;
    }

    @NonNull
    @Override
    public ActivityListAdapter.ActivityListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        binding = ActivityListItemBinding.inflate(
                LayoutInflater.from(parent.getContext()),
                parent,
                false);

        return new ActivityListAdapter.ActivityListViewHolder(binding.getRoot(), this.fm, this.ctx);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityListViewHolder holder, int position) {
        holder.bindElements(activityList.get(position), position);
    }

    @Override
    public int getItemCount() {
        if (activityList == null) {
            return 0;
        }
        return activityList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /** RecyclerView viewholder **/
    class ActivityListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Context ctx;
        private FragmentManager fm;
        private int itemPosition;

        public ActivityListViewHolder(View itemView, FragmentManager fm, Context ctx) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.ctx = ctx;
            this.fm = fm;
        }

        public void bindElements(Activity activity, int position) {
            this.itemPosition = position;

            binding.activity.setText(activity.getActivity());

            String strType = ctx.getString(R.string.list_item_type) +" "+ activity.getType();
            binding.type.setText(strType);

            String strAccessibility = ctx.getString(R.string.list_item_accessibility) +" "+ activity.getAccessibility();
            binding.accessibility.setText(strAccessibility);

            String strParticipants = ctx.getString(R.string.list_item_participants) +" "+ activity.getParticipants();
            binding.participants.setText(strParticipants);

            String strPrice = ctx.getString(R.string.list_item_price) +" "+ activity.getPrice();
            binding.price.setText(strPrice);
        }

        @Override
        public void onClick(View view) {
            Activity activity = activityList.get(this.itemPosition);

            ActivityDetailFragment fragment = new ActivityDetailFragment();

            Bundle bundle = new Bundle();
            bundle.putParcelable("activity", activity);
            fragment.setArguments(bundle);

            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.addToBackStack(null);
            ft.commit();
        }
    }
}


