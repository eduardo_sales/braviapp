# Aplicativo de sugestão de atividades

## Descrição

Este projeto tem como objetivo sugerir atividades para o usuário em momentos de tédio. 
Para isso, foi utilizada a API https://www.boredapi.com/. 
Além disso, armazena o status e tempo gasto em cada atividade selecionada pelo usuário.

## Build do projeto

Para gerar o build do projeto basta abrí-lo no Android Studio e clicar no 
menu Build -> Build Bundle(s) / APK(s)

## Bibliotecas externas utilizadas:

#### Retrofit 2 (https://square.github.io/retrofit/):

Retrofit é uma biblioteca para realizar requisições HTTP de maneira segura e eficiente. 
As vantagens de utilizar o Retrofit são:

* Projeto consistente e atualizado;
* Compatibilidade com Java e Kotlin;
* Requisições sincronas e assincronas;  
* Enfileiramento de requisições (queue);
* Uso de anotações para determinar metodos HTTP, Headers, Body;
* Possui conversores para desserialização de objetos como: Gson, Jackson, Moshi, Protobuf, 
  Wire, Simple XML, JAXB;
* Envio de binários (fotos ou videos);
* Simples configuração de certificados em conjunto com OkHttp;

#### MongoDB Realm DB (https://docs.mongodb.com/realm/sdk/android/):    
  
Realm DB é um banco de dados para aplicativos móveis, open source, mantida pela Mongo. 
Com esta lib é possível declarar relacionamento entre objetos e possui um 
gerenciador para visualização dos dados chamado Realm Studio.

As vantagens do uso do Realm DB são:

* Realizar queries via métodos como um ORM;
* Criar migrações para versionamento do banco de dados;
* Possui listeners para verificação de mudanças nas queries ou objetos (Observalidade);
* Possui servidor para sincronização de dados no MongoDB Atlas (Realm Sync);
* Implementação e utilização mais simples do que outros bancos como SQLite;
* Velocidade superior a outros bancos;
* Documentação atualizada e completa;
* Gerenciamento de contas de usuários e autenticação em vários dispositivos;
* Atualmente possui compatibilidade com: Swift, Java, Node.js, React Native, Kotlin e Flutter;

## Possíveis melhorias

Criar autenticação com redes sociais, gameficação de atividades realizadas com recompensas 
para estimular o uso do aplicativo, ranking de usuários, notificar o usuário a cada nova 
atividade recebida e quando houver atividades em andamento para finalização das mesmas.